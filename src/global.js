/**
 * @module Global
 */
import $ from 'jquery'

export default {
	/**
	 * Ajax wrapper for making backend calls.
	 * @function
	 * @param {object} options - Options for the ajax call being made.
	 * @param {boolean} noRetry - Used to prevent retrying the call.
	 * @returns {undefined}
	 */
	$ajax: function (options, noRetry) {
		// var localhost = 'http://35.162.38.115:8002'
		var localhost = 'http://games.unoapp.io'
		options.url = localhost + options.url

		$.ajax(options)
	}
}
